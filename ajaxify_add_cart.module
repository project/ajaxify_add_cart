<?php

use Drupal\commerce_order\Entity\OrderTypeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Render\Element\StatusMessages;

/**
 * Implements hook_form_BASE_FORM_ID_alter() for 'commerce_order_type_form'.
 */
function ajaxify_add_cart_form_commerce_order_type_form_alter(array &$form, FormStateInterface $form_state) {

  /** @var OrderTypeInterface $order_type */
  $order_type = $form_state->getFormObject()->getEntity();

  $form['ajaxify_add_cart'] = [
    '#type' => 'details',
    '#title' => t('Ajaxify add to cart'),
    '#description' => t('Use ajax button to add item to cart and refesh the block cart.'),
    '#weight' => 5,
    '#open' => TRUE,
    '#collapsible' => TRUE,
  ];

  $form['ajaxify_add_cart']['option_ajax_add_cart'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable Ajax add to cart'),
    '#default_value' => $order_type->getThirdPartySetting('ajaxify_add_cart', 'option_ajax_add_cart', 0),
  ];

  $form['actions']['submit']['#submit'][] = 'ajaxify_add_cart_order_type_form_submit';
}

/**
 * Form submission handler for 'commerce_order_type_form'.
 *
 * @param $form
 *   The form array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state object.
 */
function ajaxify_add_cart_order_type_form_submit($form, FormStateInterface $form_state) {
  /** @var \Drupal\commerce_order\Entity\OrderTypeInterface $order_type */
  $order_type = $form_state->getFormObject()->getEntity();
  $settings = $form_state->getValue(['ajaxify_add_cart']);
  $order_type
    ->setThirdPartySetting('ajaxify_add_cart', 'option_ajax_add_cart', $settings['option_ajax_add_cart'])
    ->save();
}

/**
 * Check if use ajax is enabled.
 *
 * @param $entity
 *   The entity.
 *
 * @return bool
 *   Whether ajaxify_add_cart is enabled.
 */
function ajaxify_add_cart_state(EntityInterface $entity) {
  // Get bundle to current store form
  $bundle = $entity->bundle();

  /** @var \Drupal\commerce_order\Entity\OrderItemTypeInterface $item_type */
  $item_type = \Drupal::entityTypeManager()
    ->getStorage('commerce_order_item_type')
    ->load($bundle);

  /** @var OrderTypeInterface $order_type */
  $order_type = \Drupal::entityTypeManager()
    ->getStorage('commerce_order_type')
    ->load($item_type->getOrderTypeId());

  return $order_type->getThirdPartySetting('ajaxify_add_cart', 'option_ajax_add_cart', 0);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function ajaxify_add_cart_form_commerce_order_item_add_to_cart_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $object = $form_state->getFormObject();
  if ($object && method_exists($object, 'getEntity')) {
    /** @var EntityInterface $entity */
    $entity = $object->getEntity();
    if ($entity && ajaxify_add_cart_state($entity)) {
      // Add use ajax to button
      $form['actions']['submit']['#ajax'] = [
        'callback' => 'ajaxify_add_cart_submit',
        'wrapper' => 'all_form',
        'effect' => 'fade',
        'progress' => [
          'type' => 'throbber',
          'message' => t('Adding to Cart'),
        ],
      ];
      $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    }
  }
}

/**
 * Ajax callback for variation product form.
 * @param array $form
 *   The form array.
 *
 * @param FormStateInterface $form_state
 *   The form state.
 *
 * @return AjaxResponse
 *   The AJAX response.
 */
function ajaxify_add_cart_submit(array $form, FormStateInterface &$form_state) {
  $render_selector = '.cart--cart-block';
  //Get html of cart block
  $html = ajaxify_add_cart_render_cart_block();
  $messages = StatusMessages::renderMessages(NULL);
  $response = new AjaxResponse();

  if (!$form_state->getErrors()) {
    // Update block cart after add an item to cart
    $response->addCommand(new HtmlCommand($render_selector, $html));
  }

  $response->addCommand(new OpenModalDialogCommand(t('Cart'), $messages));

  return $response;
}

/**
 * Render HTML of cart block
 *
 * @return string
 *   A render text.
 */
function ajaxify_add_cart_render_cart_block() {
  /** @var \Drupal\commerce_cart_blocks\Plugin\Block\CartBlock $commerce_cart_block */
  $commerce_cart_block = \Drupal::service('plugin.manager.block')
    ->createInstance('commerce_cart', []);

  return \Drupal::service('renderer')->render($commerce_cart_block->build());
}
