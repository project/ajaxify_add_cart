Commerce Ajaxify add to cart
--------------------

Maintainers:
 Marouan Hammami mh.marouan@gmail.com / https://www.drupal.org/u/mhmarouan

SUMMARY:

This module will Ajaxify the button add item to cart everywhere, refesh the block cart and a popup message will be shown after you add an item to cart.

Uses Drupal Ajax responses (https://api.drupal.org/api/drupal/core%21core.api.php/group/ajax/8.2.x).


How it works
-After enable the module, visit Commerce Configuration -> Order types.
-Edit the settings of an order type.
-Enable "Ajaxify add to cart".
-Save the configuration